%Performing calculations and generating plots for the report

clf
clear 

rc = 0.5;
ro = 3;
I = 31;
J = 31;

[phi,x,y,r,th,Dr,Dth]  = ComputePotential(rc,ro,I,J);
[phi_a,psi_a] = AnalyticalPotential(rc,x,y);
[uc,vc,up,vp] = ComputeVelocity(phi,r,th);
[u,v] = AnalyticalVelocity(rc,x,y);
[p] = ComputePressure(uc,vc); %analytical velocity can also be used as input
[Fx,Fy] = ComputeForce(p,r,th);

% Task 4a) - Potential plots
figure(1)
contourf(x,y,reshape(phi,I,J));
% title('Computed potential');
xlabel('x')
ylabel('y')
zlabel('\phi')
axis([-4 4 0 3 -4 4])
colorbar

figure(2)
contourf(x,y,reshape(phi_a,I,J));
% title('Analytical potential')
xlabel('x')
ylabel('y')
zlabel('\phi_a')
axis([-4 4 0 3 -4 4])
colorbar

figure(3)
contourf(x,y,reshape(abs(phi_a-phi),I,J));
% title('Absolute difference between analytical and computed potential')
xlabel('x')
ylabel('y')
zlabel('|\phi_a-\phi|')
colorbar


% Task 4c) - Velocity vector fields
figure(4)
quiver(x,y,uc,vc);
% title('Computed velocity');
xlabel('x')
ylabel('y')
xlim([-3 3]);

figure(5)
quiver(x,y,u,v);
% title('Analytical velocity');
xlabel('x')
ylabel('y')
xlim([-3 3]);

% Task 5b) - Pressure field
figure(6)
contourf(x,y,p);
xlabel('x')
ylabel('y')
colorbar

% Task 6c) - Plotting streamlines
N=10;
x0=-2*ones(N,1);
y0=linspace(0.1,1.9,N)';
[x_strim,y_strim,DT]=ComputeStreamlines(up,vp,r,th,x0,y0);
figure(7);
plot(x_strim,y_strim);
xlabel('x')
ylabel('y')

% Task 7a) - Pressure distribution along the cylinder surface
figure(8);
plot(x(1,:),p(1,:))
xlabel('x')
ylabel('Pressure at the cylinder surface')

% Task 7b) - velocity profile along the line segment x = 0, y ∈ [0.5, 3]
figure(9);
plot(y(:,round(I/2)),uc(:,round(I/2)))
xlabel('y')
ylabel('Velocity at x = 0')




