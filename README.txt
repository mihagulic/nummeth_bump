GIVEN:
AnalyticalPotential
AnalyticalVelocity
ComputePotential
VecPol2Cart

TODO:
GenerateMesh			OK
BoundaryIndices			OK
PartialDerivatives		OK
PolarLaplacian			OK
DirichletBC            	OK
NeumannBC               OK
BoundaryDerivatives		OK
ComputeVelocity			OK
ComputePressure			OK
ComputeForce			probably OK
ComputeStreamlines      probably OK (some math error somewhere but works when called separately)
myode45                 probably OK
PlotVelocityProfile     OK
PlotPressureDistribution    OK

