function [x,y,DT] = ComputeStreamlines(up,vp,r,th,x0,y0)
%COMPUTERSTREAMLINES with seeding points x0,y0
%   Returns the Cartesian coordinates matrix for each
%   set of seeding points and a DT vector from myode45 function

[th0,r0] = cart2pol(x0,y0);
Fu = griddedInterpolant(th',r',up'    ,'cubic','none');
Fv = griddedInterpolant(th',r',vp'./r','cubic','none');

[R,TH,DT] = myode45(Fu,Fv,r0,th0);
[x,y] = pol2cart(TH',R');

end

