function [A,b] = NeumannBC(A,b,beta,iN,dx,iG)
%NEUMANNBC Impose Neumann BC at grid points iN
%   Impose Neumann BC at boundary grid points with indices in iN

for i = iN'

    % TODO: IMPLEMENT NEUMANN BC
    A(i,i-iG) = A(i,i-iG) + A(i-iG,i);
    b(i) = b(i) + A(i-iG,i)*dx*beta;
    
    if i+iG>0 && i+iG<size(A,2)
        % Due to the above condition this is only 
        % run for iN=ii (lower boundary)and erases the
        % the link to the upper boundary.
        % TODO: ERASE LINK TO THE OPPOSITE BOUNDARY
        A(i,i+iG) = 0;
    end
    
end

end

