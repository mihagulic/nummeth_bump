function [Fx,Fy] = ComputeForce(p,r,th)
%COMPUTEFORCE acting on the inner cylinder
%   Computes the pressure force acting on the cylindrical surface r=ri,
%   th=<0,pi/2>

[ii,~,~,~,rc] = BoundaryIndices(r,th);
dth = th(1,2) - th(1,1);

% TODO: INTEGRATE THE PRESSURE OVER THE CYLINDER SURFACE TO OBTAIN THE
% TOTAL FORCE
p = p(ii);
theta = 0;
Fx = 0;
Fy = 0;
n = pi/dth;

% == midpoint rule == %
for i=1:n
    theta = dth/2 + (i-1)*dth;
    p_av = (p(i) + p(i+1))/2;
    Fx = Fx + p_av*rc*dth*cos(theta);
    Fy = Fy - p_av*rc*dth*sin(theta);
end 

% == left Riemann == %
% for i=1:n
%     theta = (i-1)*dth;
%     Fx = Fx + p(i)*rc*dth*cos(theta);
%     Fy = Fy - p(i)*rc*dth*sin(theta);
% end 

% == right Riemann == %
% for i=1:n
%     theta = i*dth;
%     Fx = Fx + p(i+1)*rc*dth*cos(theta);
%     Fy = Fy - p(i+1)*rc*dth*sin(theta);
% end

end

