function [ii,io,il,ir,rc,ro,thl,thr] = BoundaryIndices(r,th)
%BOUNDARYINDICES Find indices of boundary points
%   Finds indices of grid points belonging to different boundaries
%   

rc = r(1);
ro = r(end);
thl = th(1);
thr = th(end);

% TODO: FIND ii,io,il and ir 
ii = find( r(:)==rc );
io = find( r(:)==ro );
il = find( th(:)==0 );
ir = find( th(:)==pi );


end

