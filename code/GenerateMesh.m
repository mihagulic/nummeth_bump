function [r,th,dr,dth] = GenerateMesh(rc,ro,I,J)
%GENERATEMESH in polar coordinates and return grid spacings
%   Generates polar coordinates of uniformly distributed grid points
% IMPLEMENT YOUR CODE HERE

%compute grid spacings
dr = (ro - rc)/(I-1);
dth = pi/(J-1);

% fill the r matrix
r = ones(I,J);
for i=1:I
    r(i,:) = rc + (i-1)*dr;
end

% fill the theta matrix
th = ones(I,J);
for j=1:J
    th(:,j) = (j-1)*dth;
end

% == alternative way(more efficient) == %
% r = rc:dr:ro;
% th = 0:dth:pi;
% [th, r] = meshgrid(th, r);
end

