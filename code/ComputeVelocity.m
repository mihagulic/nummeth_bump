function [uc,vc,up,vp] = ComputeVelocity(phi,r,th)
%COMPUTEVELOCITY Compute velocity of the potential flow
%   around a cylinder in polar coordinates

[I,J] = size(r);
[Dr,Dth] = PartialDerivatives(r,th);
[Dr,Dth] = BoundaryDerivatives(r,th,Dr,Dth);

% TODO: COMPUTE THE POLAR VELOCITY COMPONENTS up AND vp

N = I*J;
up = Dr*reshape(phi,N,1); 
vp = (1./reshape(r,N,1)).*Dth*reshape(phi,N,1);

% === alternative way (should be equivalent) ==
% phi = phi(:);
% up = Dr*phi;
% vp = (1./r(:)).*Dth*phi;

up = full(reshape(up,I,J));
vp = full(reshape(vp,I,J));

[uc,vc] = VecPol2Cart(up,vp,th,r);

end

